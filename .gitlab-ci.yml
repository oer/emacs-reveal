# SPDX-FileCopyrightText: 2017-2024 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0

# Inspired by: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html
# See also:
# - https://docs.gitlab.com/ee/ci/variables/
# - https://docs.gitlab.com/ee/ci/yaml/

# CI for emacs-reveal: Tests and docker images

# Some lessons learned:
# - Avoid colons in scripts.  They confuse the YAML parser.  Quoting
#   did not work for me.
# - In the "variables:" section, variables cannot be defined in terms
#   of other (predefined) variables.  That is the reason for the
#   export command for TESTDIR below.
# - Command “docker run” requires absolute paths for volumes with “-v”.
# - Newly built Docker images are not available in subsequent stages.
#   Thus, push test image.
# - Newly built Docker images cannot be just tagged.  Pull first.
# - When building Docker images, use --cache-from to speed up job.
# - Of course, be more careful which commands are available in what
#   image :)
# - The use of variables in an artifacts path did not work for me.
#   I tried $TESTDIR/$ROBOT_HTML_DIR and $TESTDIR/$ROBOT_REPORTS_DIR,
#   which failed with empty $TESTDIR.  Thus, I use "tests" now.
# - A tag is not bound to a branch, so branch restrictions in
#   only/except do not work.
# - Using only changes to restrict Docker build needs an explicit
#   except tags, as tags seem to imply changes everywhere.
# - Newer robot-framework runs as non-root user by default, potentially
#   causing permission issues:
#   [Errno 13] Permission denied: '/opt/robotframework/reports/output.xml'
# - Trying old behavior with --user 0:0 lets Firefox fail:
#   Running Firefox as root in a regular user's session is not supported.
# - Setting permissions on parent folder of host is not good enough.
#   Thus, mkdir below first, then chown.
# - Chrome tests were successful once, then failed:
#   - Success: https://gitlab.com/oer/emacs-reveal/-/jobs/4923809904
#   - Failure: https://gitlab.com/oer/emacs-reveal/-/jobs/4923871683
#   - I added the shm-size parameter (mentioned in the README against
#     Chrome crashes), and tests succeeded again.  Luck?
#     - Success: https://gitlab.com/oer/emacs-reveal/-/jobs/4928224473
#     - In case of failures, chrome logging options from the README
#       might help...

variables:
  ROBOT_FRAMEWORK: ppodgorsek/robot-framework:6.1.0
  ROBOT_HTML_DIR: public
  ROBOT_REPORTS_DIR: reports
  ROBOT_TESTS_DIR: robotframework
  OLD_EMACS_REVEAL: registry.gitlab.com/oer/emacs-reveal/emacs-reveal:latest

stages:
  - build
  - test
  - build-docker
  - build-new
  - test-docker
  - push-docker
  - push-docker-main

.build_html: &build_html
  script:
    - make all
  artifacts:
    paths:
      - tests/public
      - docker/emacs-reveal.tar.gz
    expire_in: 20 minutes
  only:
    refs:
      - lechten
      - main
      - merge_requests
      - tags

build-html-1:
  <<: *build_html
  stage: build
  image: $OLD_EMACS_REVEAL

.docker_template: &docker_template
  image: docker:stable
  services:
    - docker:dind
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_JOB_NAME:$CI_COMMIT_TAG
    IMAGE_LATEST: $CI_REGISTRY_IMAGE/$CI_JOB_NAME:latest
    IMAGE_TEST: $CI_REGISTRY_IMAGE/${CI_JOB_NAME}_test:latest
  before_script:
    - echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY

debian-emacs-tex:
  <<: *docker_template
  stage: build
  script:
    - docker pull $IMAGE_LATEST || true
    - docker build --cache-from $IMAGE_LATEST -t $IMAGE_LATEST -f docker/$CI_JOB_NAME/Dockerfile .
    - docker push $IMAGE_LATEST
  only:
    changes:
      - docker/debian-emacs-tex/*
  except:
    - tags

selenium-chrome:
  <<: *docker_template
  stage: build
  script:
    - docker pull $IMAGE_LATEST || true
    - docker build --cache-from $IMAGE_LATEST -t $IMAGE_LATEST -f docker/$CI_JOB_NAME/Dockerfile docker
    - docker push $IMAGE_LATEST
  only:
    changes:
      - docker/selenium-chrome/*
      - docker/code/selenium-print-pdf.py
      - .gitlab-ci.yml
  except:
    - tags

tts:
  <<: *docker_template
  stage: build
  script:
    - docker pull $IMAGE_LATEST || true
    - docker build --cache-from $IMAGE_LATEST -t $IMAGE_LATEST -f docker/$CI_JOB_NAME/Dockerfile docker
    - docker push $IMAGE_LATEST
  only:
    changes:
      - docker/tts/*
      - docker/code/tts.py
      - .gitlab-ci.yml
  except:
    - tags

tts-styletts2:
  <<: *docker_template
  stage: build
  script:
    - docker pull $IMAGE_LATEST || true
    - docker build --cache-from $IMAGE_LATEST -t $IMAGE_LATEST -f docker/$CI_JOB_NAME/Dockerfile docker
    - docker push $IMAGE_LATEST
  only:
    changes:
      - docker/tts-styletts2/*
      - docker/code/tts.py
      - .gitlab-ci.yml
  except:
    - tags

test-reuse:
  stage: test
  image:
    name: fsfe/reuse:latest
    entrypoint: [""]
  script:
    - reuse lint
  except:
    - tags

.test-robot: &test-robot
  <<: *docker_template
  before_script:
    - export TESTDIR="${CI_PROJECT_DIR}/tests"
  script:
    - mkdir $TESTDIR/$ROBOT_REPORTS_DIR
    - chown -R 1000:1000 $TESTDIR
    - docker run --user 1000:1000 -v $TESTDIR/$ROBOT_REPORTS_DIR:/opt/robotframework/reports:Z -v $TESTDIR/$ROBOT_TESTS_DIR:/opt/robotframework/tests:Z -v $TESTDIR/$ROBOT_HTML_DIR:/robot/public -e BROWSER=firefox -e PRESENTATION=test.html?default-navigation $ROBOT_FRAMEWORK
    - docker run --user 1000:1000 --shm-size=1g -v $TESTDIR/$ROBOT_REPORTS_DIR:/opt/robotframework/reports:Z -v $TESTDIR/$ROBOT_TESTS_DIR:/opt/robotframework/tests:Z -v $TESTDIR/$ROBOT_HTML_DIR:/robot/public -e BROWSER=chrome -e PRESENTATION=test.html?default-navigation $ROBOT_FRAMEWORK
  artifacts:
    when: on_failure
    paths:
      - tests/$ROBOT_HTML_DIR
      - tests/$ROBOT_REPORTS_DIR
    expire_in: 1 week
  only:
    refs:
      - lechten
      - main
      - merge_requests

test-robot-1:
  <<: *test-robot
  stage: test

emacs-reveal:
  <<: *docker_template
  stage: build-docker
  script:
    - docker pull $IMAGE_TEST || true
    - docker build --cache-from $IMAGE_TEST -t $IMAGE_TEST -f docker/$CI_JOB_NAME/Dockerfile docker
    - docker push $IMAGE_TEST
  only:
    - tags
    - lechten

build-html-2:
  <<: *build_html
  stage: build-new
  image: $CI_REGISTRY_IMAGE/emacs-reveal_test:latest
  only:
    - tags
    - lechten

test-robot-2:
  <<: *test-robot
  stage: test-docker
  only:
    - lechten
    - tags

emacs-reveal-push:
  <<: *docker_template
  variables:
    GIT_STRATEGY: none
  stage: push-docker
  script:
    - docker pull $CI_REGISTRY_IMAGE/emacs-reveal_test:latest
    - docker tag $CI_REGISTRY_IMAGE/emacs-reveal_test:latest $CI_REGISTRY_IMAGE/emacs-reveal:$CI_COMMIT_TAG
    - docker push $CI_REGISTRY_IMAGE/emacs-reveal:$CI_COMMIT_TAG
  only:
    - tags

emacs-reveal-push-not-old:
  <<: *docker_template
  variables:
    GIT_STRATEGY: none
  stage: push-docker-main
  script:
    - docker pull $CI_REGISTRY_IMAGE/emacs-reveal:$CI_COMMIT_TAG
    - docker tag $CI_REGISTRY_IMAGE/emacs-reveal:$CI_COMMIT_TAG $CI_REGISTRY_IMAGE/emacs-reveal:latest
    - docker push $CI_REGISTRY_IMAGE/emacs-reveal:latest
  rules:
    - if: '$CI_COMMIT_TAG && $CI_COMMIT_TAG !~ /^7/'
