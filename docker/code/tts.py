#!/usr/bin/python
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2023,2024 Jens Lechtenbörger

"""Generate audio from input text with TTS models.

This module supports Microsoft SpeechT5, SpeechBrain using a Tacotron2
pretrained on LJSpeech, and StyleTTS2.

This file contains code fragments from there:
- https://huggingface.co/microsoft/speecht5_tts
- https://huggingface.co/spaces/Matthijs/speecht5-tts-demo
- https://huggingface.co/speechbrain/tts-tacotron2-ljspeech

The following Python packages need to be installed:
pip install git+https://github.com/huggingface/transformers sentencepiece datasets
pip install tensorflow torch soundfile torchaudio wave
pip install speechbrain

Also, you need ffmpeg.

The CMU ARCTIC dataset is the source of these speakers for SpeechT5:
CLB (US female)
SLT (US female)
BDL (US male)
RMS (US male)
KSP (Indian male)

Their files for use here are available in directory spkemb over there:
https://huggingface.co/spaces/Matthijs/speecht5-tts-demo

StyleTTS2 is available over there: https://github.com/yl4579/StyleTTS2/
This file makes use of a separate pip package:
https://github.com/sidharthrajaram/StyleTTS2
"""

from abc import ABC, abstractmethod
import array
import logging
import pathlib
import os
import re
import shutil
import subprocess
import sys
import wave
from scipy.io.wavfile import write as write_wav, read as read_wav

import numpy as np
import soundfile as sf
import torch
import torchaudio

from speechbrain.inference.TTS import Tacotron2
from speechbrain.inference.vocoders import HIFIGAN

from transformers import SpeechT5Processor
from transformers import SpeechT5ForTextToSpeech
from transformers import SpeechT5HifiGan

from styletts2 import tts


WAV_INT = 1
WAV_FLOAT = 3
LOGGER = logging.getLogger("tts")


class AbstractTTS(ABC):
    """Wrapper class for TTS implementations."""
    sampletext = "This is an example."

    break_re = re.compile(r"<break time=\"(\d+(\.\d+)?)s\" ?/>", re.I)

    @abstractmethod
    def speak_line(self, line, out_path):
        """Speak line of text to out_path."""
        raise NotImplementedError()

    @property
    def samplerate(self):
        """Get samplerate of generated audio."""
        return self._samplerate

    @property
    def sampleaudio(self):
        """Get a generated sample audio.
        Used to extract parameters of generated audio."""
        if self._sampleaudio is None:
            self._sampleaudio = os.path.join(
                self._tmp_dir, f"_sample_{self.speaker}.wav")
            self.speak_line(self.sampletext, self._sampleaudio)
        return self._sampleaudio

    @property
    def speaker(self):
        """Return speaker's name."""
        return self._speaker

    def silence(self, length, out_path):
        """Create silent audio of length seconds to out_path."""
        _silence = self.create_silence(length)
        if self._wavtype == WAV_INT:
            with wave.open(out_path, "wb") as wav_out:
                with wave.open(self.sampleaudio, "rb") as wav_in:
                    wav_out.setparams(wav_in.getparams())
                    wav_out.writeframes(_silence)
        else:
            write_wav(out_path, self.samplerate, _silence)

    @abstractmethod
    def __init__(self, speaker, samplerate, parts,
                 tmp_dir="tmp", wav_type=WAV_INT):
        """Initialize self for speaker and samplerate.
        Set configuration for generated audio based on parts and tmp_dir.
        Currently, parts specify an inter-sentence gap/silence and optionally
        gaps at the start and end of audio files."""
        super().__init__()
        self._speaker = speaker
        self._samplerate = samplerate
        self._sampleaudio = None
        self._wavtype = wav_type
        if not os.path.exists(tmp_dir):
            os.mkdir(tmp_dir)
        self._tmp_dir = tmp_dir
        self._parts = [part.strip() for part in parts]
        self.gap_lengths = {}
        self.gap_lengths["sentence"] = float(self._parts[0])
        self.gap_lengths["start"] = 0.0
        self.gap_lengths["end"] = 0.0
        if len(parts) > 1:
            self.gap_lengths["start"] = float(self._parts[1])
            if len(parts) > 2:
                self.gap_lengths["end"] = float(self._parts[2])
        self.gaps = {}
        for key, length in self.gap_lengths.items():
            if length > 0:
                self.gaps[key] = self.create_silence(length)

    def conf_str(self):
        """Return string that represents configuration."""
        return self.speaker + "-" + "-".join(self._parts)

    def create_silence(self, length):
        """Create silent audio data with given length in seconds."""
        silence_frames = int(length * self.samplerate)
        if self._wavtype == WAV_INT:
            return array.array('h', [0] * silence_frames).tobytes()
        return np.zeros(silence_frames)

    def concat_audios(self, audios, target):
        """Concatenate files in audios to build target.
        Potentially add silence at start and end as well as between
        individual audios."""
        pieces = []
        if "start" in self.gaps:
            pieces.append(self.gaps["start"])
        for audio in audios:
            if self._wavtype == WAV_INT:
                with wave.open(audio, "rb") as wav_in:
                    data = wav_in.readframes(wav_in.getnframes())
            else:
                _, data = read_wav(audio)
            pieces.append(data)
            if "sentence" in self.gaps:
                pieces.append(self.gaps["sentence"])
        if "end" in self.gaps:
            pieces.append(self.gaps["end"])
        if self._wavtype == WAV_INT:
            # Recipe: https://stackoverflow.com/a/49455325
            with wave.open(target, "wb") as wav_out:
                with wave.open(self.sampleaudio, "rb") as wav_in:
                    wav_out.setparams(wav_in.getparams())
                for piece in pieces:
                    wav_out.writeframes(piece)
        else:
            write_wav(target, self.samplerate, np.concatenate(pieces))

    def tts_file(self, text_file, out_prefix, out_dir):
        """Generate audio for text_file.
        For each line of text, create one audio file with out_prefix in
        out_dir.  Return list of generated audio file names."""
        result = []
        lineno = 0
        if not os.path.exists(out_dir):
            os.mkdir(out_dir)
        with open(text_file, "r", encoding="utf-8") as text:
            for line in text.readlines():
                lineno = lineno + 1
                out_path = os.path.join(
                    self._tmp_dir, f"{out_prefix}-{lineno:03d}.wav")
                break_match = self.break_re.search(line)
                if break_match is not None:
                    length = float(break_match.group(1))
                    self.silence(length, out_path)
                else:
                    self.speak_line(line, out_path)
                result.append(out_path)
        return result


class SpeechBrain(AbstractTTS):
    """Configuration for speechbrain.
    Note that local models are used.  Clone first.

    Pass constructor's arguments parts and tmp_dir to the superclass.
    """

    def __init__(self, parts, tmp_dir):
        super().__init__("speechbrain", 22050, parts, tmp_dir)
        data = os.path.dirname(os.path.realpath(__file__))
        self.tacotron2 = Tacotron2.from_hparams(
            source=os.path.join(data, "tts-tacotron2-ljspeech"),
            savedir="tmpdir_tts")
        self.hifi_gan = HIFIGAN.from_hparams(
            source=os.path.join(data, "tts-hifigan-ljspeech"),
            savedir="tmpdir_vocoder")

    def speak_line(self, line, out_path):
        """Speak line to out_path."""
        mel_output, _, _ = self.tacotron2.encode_text(line)
        waveforms = self.hifi_gan.decode_batch(mel_output)
        LOGGER.debug("Writing %s", out_path)
        torchaudio.save(
            out_path, waveforms.squeeze(1), 22050,
            bits_per_sample=16, encoding="PCM_S")


class SpeechT5(AbstractTTS):
    """Configuration for SpeechT5.
    Note that local models are used.  Clone first.

    Speaker data from there:
    https://huggingface.co/spaces/Matthijs/speecht5-tts-demo

    Pass constructor's arguments speaker, parts, tmp_dir to the superclass.
    """

    speakers = {
        "BDL": "spkemb/cmu_us_bdl_arctic-wav-arctic_a0009.npy",
        "CLB": "spkemb/cmu_us_clb_arctic-wav-arctic_a0144.npy",
        "KSP": "spkemb/cmu_us_ksp_arctic-wav-arctic_b0087.npy",
        "RMS": "spkemb/cmu_us_rms_arctic-wav-arctic_b0353.npy",
        "SLT": "spkemb/cmu_us_slt_arctic-wav-arctic_a0508.npy",
    }

    def __init__(self, speaker, parts, tmp_dir):
        super().__init__(speaker, 16000, parts, tmp_dir)
        data = os.path.dirname(os.path.realpath(__file__))
        self.processor = SpeechT5Processor.from_pretrained(
            os.path.join(data, "speecht5_tts"))
        self.model = SpeechT5ForTextToSpeech.from_pretrained(
            os.path.join(data, "speecht5_tts"))
        self.vocoder = SpeechT5HifiGan.from_pretrained(
            os.path.join(data, "speecht5_hifigan"))
        speaker = np.load(os.path.join(data, self.speakers[speaker]))
        self.speaker_embedding = torch.tensor(speaker).unsqueeze(0)

    def speak_line(self, line, out_path):
        """Speak line to out_path."""
        if len(line) > self.model.config.max_text_positions:
            LOGGER.warning("Line too long, only %d chars used: %s",
                           self.model.config.max_text_positions, line)

        inputs = self.processor(text=line, return_tensors="pt")
        input_ids = inputs["input_ids"]
        input_ids = input_ids[..., :self.model.config.max_text_positions]
        speech = self.model.generate_speech(
            input_ids, self.speaker_embedding, vocoder=self.vocoder)

        LOGGER.debug("Writing %s", out_path)
        sf.write(out_path, speech.numpy(), samplerate=self.samplerate)


class StyleTTS2(AbstractTTS):
    """Configuration for StyleTTS2.
    Pass constructor's arguments parts and tmp_dir to the superclass.
    The target_voice, if not None, is the path to a wav audio for
    voice cloning.
    """

    def __init__(self, parts, tmp_dir, target_voice):
        name = "StyleTTS2"
        if target_voice is not None:
            voice = os.path.splitext(os.path.basename(target_voice))[0]
            name = f"{name}-{voice}"
        super().__init__(name, 24000, parts, tmp_dir, WAV_FLOAT)
        self.styletts2 = tts.StyleTTS2()
        self.target_voice = target_voice

    def speak_line(self, line, out_path):
        """Speak line to out_path."""
        self.styletts2.inference(
            line,
            target_voice_path=self.target_voice,
            output_wav_file=out_path)
        LOGGER.debug("Written %s", out_path)


def wav_to_ogg(wav_file, ogg_file):
    """Create ogg_file from wav_file with ffmpeg.
    Use hardcoded options for higher quality."""
    proc = subprocess.run(
        ["ffmpeg", "-y", "-i", wav_file,
         "-ab", "64000", "-ar", "22050", ogg_file], check=False)
    if proc.returncode == 0:
        LOGGER.debug("Created ogg audio: %s", ogg_file)
    else:
        LOGGER.warning("ffmpeg failed to create: %s", ogg_file)


def process_index(index_file, out_dir, cache_dir=None, tmp_dir="tmp"):
    """Create audio files in out_dir from index_file.
    If an audio file exists in cache_dir, do not create it
    but copy it to out_dir.
    See main() for format of index_file."""
    with open(index_file, "r", encoding="utf-8") as index:
        parts = index.readline().split(" ")
        speaker = parts[0].strip()

        if speaker == "speechbrain":
            tts = SpeechBrain(parts[1:], tmp_dir)
        elif speaker.startswith("StyleTTS2"):
            # Docker image tts-StyleTTS2 contains that voice:
            # https://styletts2.github.io/wavs/LJSpeech/OOD/GT/00001.wav
            target_voice = "/tts/StyleTTS2/LJSpeech-GT1.wav"
            speaker_parts = speaker.split(":")
            if len(speaker_parts) > 1:
                target_voice = speaker_parts[1]
            if not os.path.isfile(target_voice):
                LOGGER.error(
                    "Target voice file for StyleTTS2 does not exist: %s",
                    target_voice)
                target_voice = None
            LOGGER.debug(
                "Using StyleTTS2 with target voice: %s", target_voice)
            tts = StyleTTS2(parts[1:], tmp_dir, target_voice)
        else:
            tts = SpeechT5(speaker, parts[1:], tmp_dir)

        for line in index.readlines():
            path, md5 = [part.strip() for part in line.split(" ")]
            stem = f"{md5}-{tts.conf_str()}"
            out_prefix = os.path.join(out_dir, stem)
            wav_file = out_prefix + ".wav"
            ogg_file = out_prefix + ".ogg"
            if cache_dir is not None:
                cache_prefix = os.path.join(cache_dir, stem)
                cache_file = cache_prefix + ".ogg"
                if os.path.isfile(cache_file):
                    LOGGER.debug("Copying %s.", cache_file)
                    shutil.copy2(cache_file, ogg_file)
            md5_file = os.path.join(os.path.dirname(index_file), md5)
            if os.path.isfile(ogg_file):
                LOGGER.debug("Skipping TTS for %s, ogg exists.", ogg_file)
            else:
                audios = tts.tts_file(md5_file, stem, out_dir)
                tts.concat_audios(audios, wav_file)
                LOGGER.debug("Created wav audio: %s", wav_file)
                wav_to_ogg(wav_file, ogg_file)
            audio_link = os.path.join(out_dir, path + ".ogg")
            if os.path.islink(audio_link):
                # An existing symlink may point to an old ogg file (e.g.,
                # different speaker).  Thus, remove first, then create.
                os.remove(audio_link)
            try:
                os.symlink(os.path.basename(ogg_file), audio_link)
            except:
                LOGGER.warning(
                    "Creation of symbolic link for %s failed!", audio_link)
                shutil.copyfile(ogg_file, audio_link)
                LOGGER.warning(
                    "Copied %s to %s (instead of symbolic link).",
                    ogg_file, audio_link)


def main(index_dir, out_dir, cache_dir=None):
    """Perfom TTS for index files in index_dir to create audios in out_dir.
    Directoy cache_dir (if not None) specifies a location to look for
    preexisting audio files (which are just copied instead of created).
    Index files are those with extension ".tts", of the following format:
    The first line contains space-sparated fields that specify:
    The speaker, either "speechbrain", a speaker in SpeechT5.speakers, or
    "StyleTTS2", potentially in the form "StyleTTS2:/path/to/target_voice.wav"
    for voice cloning based on the specified target voice,
    followed by gap/silence specifications in seconds:
    an inter-sentence gap, optionally followed by gaps at the start and
    the end of generated audio.
    Further lines consist of two space-separated fields:
    A name prefix for the target audio to be generated and the name
    of the file containing its text."""
    index_paths = pathlib.Path(index_dir).glob("**/*.tts")
    for index in index_paths:
        LOGGER.debug("Processing: %s", index)
        process_index(str(index), out_dir, cache_dir)


if __name__ == "__main__":
    OUTDIR = "."
    LOGLEVEL = logging.DEBUG
    CACHEDIR = os.path.join("old-artifacts", "public", "audio")
    if len(sys.argv) > 1:
        if len(sys.argv) > 2:
            OUTDIR = sys.argv[2]
            if len(sys.argv) > 3:
                LOGLEVEL = sys.argv[3]
        LOGGER.setLevel(LOGLEVEL)
        HANDLER = logging.StreamHandler()
        FORMATTER = logging.Formatter(
            fmt="%(asctime)s tts.py %(levelname)-8s %(message)s",
            datefmt="%Y-%m-%d %H:%M:%S")
        HANDLER.setFormatter(FORMATTER)
        LOGGER.addHandler(HANDLER)
        LOGGER.propagate = False
        main(sys.argv[1], OUTDIR, CACHEDIR)
    else:
        print(f"Usage: <{sys.argv[0]}> index_dir [out_dir] [loglevel]")
