#!/usr/bin/python
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2024 Jens Lechtenbörger

# Trigger downloads for StyleTTS2.

import logging
import nltk
import sys
from styletts2 import tts

nltk.download("punkt")
nltk.download("punkt_tab")

logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                    format="%(asctime)s %(levelname)-8s %(message)s",
                    datefmt="%Y-%m-%d %H:%M:%S")
styletts2 = tts.StyleTTS2()
