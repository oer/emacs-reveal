# SPDX-FileCopyrightText: 2023 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0

# CPU variant of PyTorch.
# Location from there: https://pytorch.org/get-started/locally/
--index-url https://download.pytorch.org/whl/cpu
torch
torchaudio
